//
//  AppShareData.swift
//  MyARApplication
//
//  Created by Apple on 10/04/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class AppShareData: NSObject {
    var timerTest : Timer? = nil

    
    
    //MARK: - Shared object
    private static var sharedManager: AppShareData = {
        let manager = AppShareData()
        return manager
    }()
    
    // MARK: - Accessors
    class func sharedObject() -> AppShareData {
        return sharedManager
    }
    
}
