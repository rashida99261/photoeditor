//
//  saveImageViewController.swift
//  MyARApplication
//
//  Created by Reinforce on 01/04/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class saveImageViewController: UIViewController {
    
    @IBOutlet weak var saveImg : UIImageView!
    var saveimg : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(saveImg)
        saveImg.image = saveimg
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func clikcOnHomeBtn(_ sender : UIButton){
        isEditImage = true
        let home = self.storyboard?.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
       // self.navigationController?.pushViewController(home, animated: true)
        self.present(home, animated: false, completion: nil)
    }
    
    @IBAction func clickOnConEditing(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}
