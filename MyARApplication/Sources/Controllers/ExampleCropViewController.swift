//
//  ExampleCropViewController.swift
//  IGRPhotoTweaks
//
//  Created by Vitalii Parovishnyk on 2/7/17.
//  Copyright © 2017 IGR Software. All rights reserved.
//

import IGRPhotoTweaks

import UIKit

class ExampleCropViewController: IGRPhotoTweakViewController {
    
    /**
     Slider to change angle.
     */
    @IBOutlet weak fileprivate var angleSlider: UISlider?
    @IBOutlet weak fileprivate var angleLabel: UILabel?
    @IBOutlet weak fileprivate var img: UIImageView?
    
    
    // MARK: - Life Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupSlider()
    
        //FIXME: Zoom setup
        //self.photoView.minimumZoomScale = 1.0;
        //self.photoView.maximumZoomScale = 10.0;
    }
    
    //FIXME: Themes Preview
//    override open func setupThemes() {
//
//        IGRCropLine.appearance().backgroundColor = UIColor.green
//        IGRCropGridLine.appearance().backgroundColor = UIColor.yellow
//        IGRCropCornerView.appearance().backgroundColor = UIColor.purple
//        IGRCropCornerLine.appearance().backgroundColor = UIColor.orange
//        IGRCropMaskView.appearance().backgroundColor = UIColor.blue
//        IGRPhotoContentView.appearance().backgroundColor = UIColor.gray
//        IGRPhotoTweakView.appearance().backgroundColor = UIColor.brown
//    }
    
    fileprivate func setupSlider() {
        self.angleSlider?.minimumValue = -Float(IGRRadianAngle.toRadians(90))
        self.angleSlider?.maximumValue = Float(IGRRadianAngle.toRadians(90))
        self.angleSlider?.value = 0.0
        
        setupAngleLabelValue(radians: CGFloat((self.angleSlider?.value)!))
    }
    fileprivate func setupAngleLabelValue(radians: CGFloat) {
        let intDegrees: Int = Int(IGRRadianAngle.toDegrees(radians))
        self.angleLabel?.text = "\(intDegrees)°"
    }
    
    // MARK: - Rotation
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context) in
            self.view.layoutIfNeeded()
        }) { (context) in
            //
        }
    }
    
    // MARK: - Actions
    
   
    
    @IBAction func onEndTouchAngleControl(_ sender: UIControl) {
        self.stopChangeAngle()
    }
    
   
    
    @IBAction func onTouchCancelButton(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "", message: "Exit without saving changes?", preferredStyle: .alert)
        
        let alertOK = UIAlertAction(title: "Exit", style: .default, handler: { action in
            switch action.style{
            case .default:
                
                UserDefaults.standard.set("", forKey: "MoreTool")
                UserDefaults.standard.synchronize()
                
                self.dismissAction()

            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
            }})
        
        let alertCancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default: break
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }})
        
        alert.addAction(alertOK)
        alert.addAction(alertCancel)
        self.present(alert, animated: true, completion: nil)
}
    
    @IBAction func onTouchCropButton(_ sender: UIButton) {
        cropAction()
    }
    
    @IBAction func onTouchAspectButton(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil,
                                            message: nil,
                                            preferredStyle: .actionSheet)
        
        
        actionSheet.addAction(UIAlertAction(title: "Original", style: .default) { (action) in
            self.resetAspectRect()
        })
        
        actionSheet.addAction(UIAlertAction(title: "1:1", style: .default) { (action) in
            self.setCropAspectRect(aspect: "1:1")
        })
        
        actionSheet.addAction(UIAlertAction(title: "1:1.91", style: .default) { (action) in
            self.setCropAspectRect(aspect: "1:1.91")
        })
        
        actionSheet.addAction(UIAlertAction(title: "4:5", style: .default) { (action) in
            self.setCropAspectRect(aspect: "4:5")
        })
        
        actionSheet.addAction(UIAlertAction(title: "16:9", style: .default) { (action) in
            self.setCropAspectRect(aspect: "16:9")
        })
        
        actionSheet.addAction(UIAlertAction(title: "3:4", style: .default) { (action) in
            self.setCropAspectRect(aspect: "3:4")
        })
        
        actionSheet.addAction(UIAlertAction(title: "4:3", style: .default) { (action) in
            self.setCropAspectRect(aspect: "4:3")
        })
        
        actionSheet.addAction(UIAlertAction(title: "5:7", style: .default) { (action) in
            self.setCropAspectRect(aspect: "5:7")
        })
        
        actionSheet.addAction(UIAlertAction(title: "9:16", style: .default) { (action) in
            self.setCropAspectRect(aspect: "9:16")
        })
        
        actionSheet.addAction(UIAlertAction(title: "X:Y", style: .default) { (action) in
           //
            
            self.clickMnaulRatio()
        })
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func onTouchLockAspectRatioButton(_ sender: UISwitch) {
        self.lockAspectRatio(sender.isOn)
    }
    
    func clickMnaulRatio(){
        
        let alertController = UIAlertController(title: "Enter Ratio", message: "", preferredStyle: .alert)
        
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter X"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Y"
        }
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            self.setCropAspectRect(aspect: "\(firstTextField.text!):\(secondTextField.text!)")
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //FIXME: Themes Preview
//    override open func customBorderColor() -> UIColor {
//        return UIColor.red
//    }
//
//    override open func customBorderWidth() -> CGFloat {
//        return 2.0
//    }
//
//    override open func customCornerBorderWidth() -> CGFloat {
//        return 4.0
//    }
//
//    override open func customCropLinesCount() -> Int {
//        return 3
//    }
//
//    override open func customGridLinesCount() -> Int {
//        return 4
//    }
//
//    override open func customCornerBorderLength() -> CGFloat {
//        return 30.0
//    }
//
//    override open func customIsHighlightMask() -> Bool {
//        return true
//    }
//
//    override open func customHighlightMaskAlphaValue() -> CGFloat {
//        return 0.3
//    }
    
    override open func customCanvasHeaderHeigth() -> CGFloat {
        var heigth: CGFloat = 0.0
        
        if UIDevice.current.orientation.isLandscape {
            heigth = 40.0
        } else {
            heigth = 100.0
        }
        
        return heigth
    }
    
    // MARK: - Actions
    
    @IBAction func onChandeAngleSliderValue(_ sender: UISlider) {
        let radians: CGFloat = CGFloat(sender.value)
        setupAngleLabelValue(radians: radians)
        //self.changeAngle(radians: radians)
    }
    
//    @IBAction func onEndTouchAngleControl(_ sender: UIControl) {
//        self.stopChangeAngle()
//    }
    
}

