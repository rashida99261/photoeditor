//
//  PhotoEditorViewController.swift
//  MyARApplication
//
//  Created by Apple on 01/04/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class PhotoEditorViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        if isEditImage{
            isEditImage = false
          selectedIndex = 2
        }else{
            selectedIndex = 1
        }
    }
    
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
  
    }

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item")
        if selectedIndex == 0{
            if count == 0 {
                isSelectedIcon = true
                stopTimer()
            }
        }else if selectedIndex == 1{
            //timerTest?.invalidate()
            if count == 0 {
                isSelectedIcon = true
               stopTimer()
            }        }else if selectedIndex == 2{
            if count == 0 {
                isSelectedIcon = true
               stopTimer()
            }

        }else if selectedIndex == 3{
            if count == 0 {
                isSelectedIcon = true
                stopTimer()
            }

        }else if selectedIndex == 4{
            if count == 0 {
                isSelectedIcon = true
                stopTimer()
            }

        }
    }
    
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
