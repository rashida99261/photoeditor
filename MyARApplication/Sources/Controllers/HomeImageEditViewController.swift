//
//  HomeImageEditViewController.swift
//  MyARApplication
//
//  Created by Apple on 30/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Photos

class HomeImageEditViewController: UIViewController {
    @IBOutlet weak var imgGif: UIImageView!
    let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        imgGif.image = UIImage.gifImageWithName("plus")


    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    @IBAction func clickOnBtnOpen(_ sender: UIButton)
    {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
               
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
                   
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
                   
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
                   
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

//button Action
extension HomeImageEditViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLImageEditorDelegate{
    
   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        let editor = CLImageEditor(image: image, delegate: self)
        picker.pushViewController(editor!, animated: true)
        
    }
    
    func imageEditor(_ editor: CLImageEditor!, didFinishEditingWith image: UIImage!) {
           
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
               
    }
    
     //MARK: - Add image to Library
        @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {

            if let error = error {
                self.popupAlert(title: "Error!", message: error.localizedDescription, actionTitles: ["Ok"], actions:[{action1 in
                    },{action2 in
                    }, nil])
               
            } else {
                
                dismiss(animated: false, completion: nil)
                let alert = UIAlertController(title: "Alert", message: "Your image is save in your device", preferredStyle: .alert)
                
                let alertOK = UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        
                        let save = self.storyboard?.instantiateViewController(withIdentifier: "saveImageViewController") as! saveImageViewController
                        save.saveimg = image
                        save.modalPresentationStyle = .fullScreen
                        self.present(save, animated: false, completion: nil)
                        
                        break
                      
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                    }})
                
                alert.addAction(alertOK)
                self.present(alert, animated: true, completion: nil)
                
                
           
                
        }
    }
}
