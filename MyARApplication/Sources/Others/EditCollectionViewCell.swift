//
//  EditCollectionViewCell.swift
//  MyARApplication
//
//  Created by Apple on 30/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class EditCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgEditIcon: UIImageView!
    
}
